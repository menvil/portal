<?php

class Form_addNews extends Twitter_Form
{


    protected $_textareaRows = 10;
    protected $_textareaCols = 100;


	public function __construct()
	{
		
		$this->setName('form_add_news');
        $this->setAttrib("class","form-horizontal");
        $this->setAttrib('enctype', 'multipart/form-data');

        parent::__construct();

        $title = new Zend_Form_Element_Text('title');
        $title->setLabel('Название новости')->addFilter('StringTrim')->addValidator('NotEmpty');
        $title->setAttrib('size', '40');

        $picture = new Zend_Form_Element_File('picture');
        $picture->setLabel('Изображение')
                ->setDestination(realpath(APPLICATION_PATH). '/../public/images/upload/');

        $picture->addValidator('Size', false, 1002400);
        $picture->addValidator('ImageSize', false,
                                array('minwidth'=>40,
                                      'maxwidth'=>3000,
                                      'minheight'=>100,
                                      'maxheight'=>2000
                                )
                              );
        $picture->addValidator('Count', false, array('min'=>0, 'max'=>1));
        $picture->addValidator('Extension', false, 'jpg,png,gif');
        $picture->setMultiFile(1);

        $description = new Zend_Form_Element_Textarea('description');

        $description->setLabel('Новость')->addFilter('StringTrim')->addValidator('NotEmpty');
        $description->setOptions(array('rows'=>$this->_textareaRows,  'cols'=>$this->_textareaCols));

		$submit = new Zend_Form_Element_Submit('submit');
		$submit	->setLabel("Сохранить");
		
		
		$this->addElements(array($title, $picture, $description, $submit));
		
	}
}