﻿<?php
class Form_Flats extends Twitter_Form
{
	

        
		protected $_textareaRows = 6;
		protected $_textareaCols = 50;
        
	public function __construct()
	{
		
		$this->setName('form_flats');
        $this->setAttrib("class","form-horizontal");


        parent::__construct();

        $title = new Zend_Form_Element_Text('title');
        $title->setLabel('Название продукта')->addFilter('StringTrim')->addValidator('NotEmpty');
        $title->setAttrib('size', '40');

        $description = new Zend_Form_Element_Textarea('description');
		$description->setLabel('Описание')
					->addFilter('StringTrim');	
		$description->setOptions(array('rows'=>$this->_textareaRows, 'cols'=>$this->textareaCols));							

        $status = new Zend_Form_Element_Select('status');
        $status->setLabel('Статус')
				->setRequired(true);
		$status->setMultiOptions(array('1'=>'активен','0'=>'недоступен'));

		$images = new Zend_Form_Element_File('images');
		
		$images->setLabel('Изображения:')
		        ->setDestination(realpath(APPLICATION_PATH). '/../public/images/upload/');
		// ensure minimum 1, maximum 3 files
		$images->addValidator('Count', false, array('min' => 0, 'max' => 10));
		// limit to 100K
		$images->addValidator('Size', false, 902400);
		$images->addValidator('ImageSize', false,
                      array('minwidth' => 40,
                            'maxwidth' => 4000,
                            'minheight' => 100,
                            'maxheight' => 6000)
                      );
		// only JPEG, PNG, and GIFs
		$images->addValidator('Extension', false, 'jpg,png,gif');
		
		// defines 3 identical file elements
		$images->setMultiFile(3);

		$submit = new Zend_Form_Element_Submit('submit');
		$submit	->setLabel("Сохранить");


		$this->addElements(array($title, $description, $status, $images, $submit));
	}
}