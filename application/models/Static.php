<?php

class Model_Static extends Menvil_Model{

    public function __construct ($id = null){
        parent::__construct(new Model_DbTable_Statics, $id);
    }

    public function populateForm(){
        return $this->_row->toArray();
    }

    public function getAllStatics(){
        return $this->_dbTable->fetchAll($this->_dbTable->select()->where('status = 1'));
    }

    public function getStaticsPairs(){

        $full_landings = $this->getAllStatics();
        $array_pairs = array();
        foreach($full_landings as $land)
            $array_pairs[$land['id']] = $land['name'];
        return $array_pairs;

    }

    public function getStatic($status = null)
    {

        $select = $this->_dbTable->select()
            ->from(array('q'=>$this->_dbTable->getName()));
        if(Zend_Auth::getInstance()->getIdentity()->role != 'admin')
            $select->where('q.status = ?',1);

        return $select ;

    }
}