<?php

class Model_DbTable_Statics extends Zend_Db_Table_Abstract {

    protected $_name = 'static';

    public function getName(){
        return $this->_name;
    }

}