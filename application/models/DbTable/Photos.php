<?php

class Model_DbTable_Photos extends Zend_Db_Table_Abstract {
	
	protected $_name = 'photos';

    public function getName(){
        return $this->_name;
    }

	protected $_referenceMap = array(
		'Flat' => array(
			'columns' => 'flat_id',
			'refTableClass' => 'Model_DbTable_Flats',
			'refColumns' => 'flat_id',
			'onDelete' => self::CASCADE
		)
	);

}