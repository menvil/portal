<?php

class Model_Flat extends Menvil_Model{

	public function __construct ($id = null){
		parent::__construct(new Model_DbTable_Flats, $id);
	}
	
	public function populateForm(){
		return $this->_row->toArray();	
	}
	
	public function getAllFlats(){
		return $this->_dbTable->fetchAll();
	}

    public function getFlats()
    {

        $select = $this->_dbTable->select()
            ->from(array('q'=>$this->_dbTable->getName()));

        return $select ;

    }


    public function getFlatsPhotos()
    {

        $select = $this->_dbTable->select()
            ->setIntegrityCheck(false)
            ->from(array('q'=>$this->_dbTable->getName()))
            ->joinLeft(array('p'=>'photos'), 'q.flat_id=p.flat_id', 'photo')
            ->group(array('q.flat_id'));

        return $this->_dbTable->fetchAll($select) ;

    }

	public function getPhotos(){
		return $this->_row->findDependentRowset(
			new Model_DbTable_Photos,
			'Flat'
			);
	}
	

	
}