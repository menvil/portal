<?php

class Model_Photo extends Menvil_Model{
	
	public function __construct ($id = null){
		parent::__construct(new Model_DbTable_Photos, $id);
	}
	
	public function getFlat(){
		return $this->_row->findParentRow(new Model_DbTable_Flats, 'Flat'); 
	}
	
}