<?php

class CronController extends Zend_Controller_Action
{
 	
	public function init()
    {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

    }


    public function paymentsAction(){
        $users = new Model_Owner();
        $userlist = $users->getAllUsers(1);

        foreach ($userlist as $l){
            $balance = $users->getUserBalance($l['owner_id']);

            if($balance > 0){
                $payment = new Model_Payments();
                $payment->owner_id = $l['owner_id'];
                $payment->date = date('Y-m-d H:i:s');
                $payment->amount = $balance;
                $payment->status = 'Pending';
                $payment->save();

            }

        }
        return true;

    }

    public function newtableAction(){

        $db = Zend_Registry::get('db');

        $tomorrow  = date('dmY',mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")));

        $stmt = $db->prepare(" CREATE TABLE IF NOT EXISTS `".$tomorrow."_statistic` (
                                 `Id` int(11) NOT NULL AUTO_INCREMENT,
                                 `owner_id` bigint(20) unsigned NOT NULL,
                                 `lead_id` bigint(20) unsigned DEFAULT NULL,
                                 `ip` varchar(16) NOT NULL,
                                 `useragent` varchar(100) NOT NULL,
                                 `date` datetime NOT NULL,
                                 PRIMARY KEY (`Id`),
                                 KEY `owner_id` (`owner_id`),
                                 KEY `lead_id` (`lead_id`)
                                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1
                             ");
        $stmt->execute();

        $this->statsAction(date('dmY',mktime(0, 0, 0, date("m")  , date("d")-1, date("Y"))));

        return true;
    }


    public function statsAction($date = null){

        if($date == null)
            $date = date('dmY', mktime(0, 0, 0, date("m"), date("d"), date("Y")));

        $invert_date = $date[4].$date[5].$date[6].$date[7].'-'.$date[2].$date[3].'-'.$date[0].$date[1];

        $db = Zend_Registry::get('db');

        $i = 0;

        $count = 0;
        $limit = 1000;

        do {
            $i++;
            $select = $db->select()
                ->from(array('q'=>$date.'_statistic'),
                        array('owner_id'=>'owner_id',
                                'lead_id'=>'lead_id',
                                'date'=> new Zend_Db_Expr("DATE_FORMAT('".$invert_date."', '%Y-%m-%d' )"),
                                'unique_visitors'=> new Zend_Db_Expr('COUNT( DISTINCT ip )'),
                                'all_visitors' => new Zend_Db_Expr('COUNT( ip )')))
                ->group('q.lead_id')
                ->group('q.owner_id')
                ->limitPage($i, $limit);

            $stmt = $db->query($select);
            $result = $stmt->fetchAll();

            foreach($result as $res){

                $stmt = $db->prepare("INSERT INTO statistics SET
                            visitors_total = :visitors_total, uniques = :uniques,
                            day = :date, landing_id = :lead_id, owner_id = :owner_id
                            ON DUPLICATE KEY UPDATE
                            visitors_total = :visitors_total, uniques = :uniques");

                $stmt->bindParam('date', $res['date']);
                $stmt->bindParam('owner_id', $res['owner_id']);
                $stmt->bindParam('lead_id', $res['lead_id']);
                $stmt->bindParam('visitors_total', $res['all_visitors']);
                $stmt->bindParam('uniques', $res['unique_visitors']);
                $stmt->execute();
            }
        } while(count($result) != 0);
        return true;
    }

}
