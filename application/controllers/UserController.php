<?php

class UserController extends Zend_Controller_Action
{

    public function init()
    {

        $this->view->tab = $this->_getParam('action');

        if ($this->_request->isXmlHttpRequest()) {

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

        }

    }



    public function loginAction(){
        $session = new Zend_Session_Namespace('Messages');
        $this->view->title = "Вход";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $form = new Form_Login();
        if($this->getRequest()->isPost()){
            if($form->isValid($this->getRequest()->getPost())){
                $user = new Model_Owner();
                if($user->authorize($form->getValue('username'), $form->getValue('password'))){
                    if(Zend_Auth::getInstance()->getIdentity()->role == 'operator')
                      $this->_helper->redirector('news');
                    else
                        $this->_helper->redirector('news');


                } else {
                    $this->view->error = 'Неверные данные авторизации.';
                }
            }
        }

        $this->view->msg = $session->msg;
        $session->msg = '';
        $this->view->form = $form;
    }

    public function indexAction()
    {

        $search_form = new Zend_Session_Namespace('search_form');
        $form_search = new Form_Searches();

        $this->view->title = "Статистика";
        $this->view->headTitle($this->view->title, 'PREPEND');



        if($this->getRequest()->isPost()){
            if($form_search->isValid($this->getRequest()->getPost()))
                $search_form->_data = $this->getRequest()->getParams();
            $this->_helper->redirector('index');
        }



        if(Zend_Auth::getInstance()->getIdentity()->role != 'admin')
            $select->where('q.owner_id = ?', Zend_Auth::getInstance()->getIdentity()->owner_id);


        //$select->where('q.owner_id = ?',1589);

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($select));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;

        $owner = new Model_Owner();
        $owner->getUserBalance(Zend_Auth::getInstance()->getIdentity()->owner_id);
        $this->view->users = $owner->getJsUsers();
        $this->view->form_search = $form_search;

    }


    public function newsAction(){

        $this->view->title = "Новости";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $news = new Model_News();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($news->getNews()));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;

    }

    public function staticsAction(){

        $this->view->title = "Статические страницы";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $statics = new Model_Static();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($statics->getStatic()));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;

    }

    public function addstaticAction(){
        $this->view->title = "Редактирование страницы";
        $this->view->headTitle($this->view->title, 'PREPEND');
        $session = new Zend_Session_Namespace('Messages');

        $news = new Model_Static();
        $form = new Form_addStatic();


        if($this->getRequest()->isPost()){

            if($form->isValid($this->getRequest()->getPost()) &&  !isset($this->_request->id)){

                $news->fill($form->getValues());
                $news->date  = date('Y-m-d H:i:s');
                $news->save();
                $session->msg = 'Ваши данные изменены';
                $this->_helper->redirector('statics');

            } elseif ($form->isValid($this->getRequest()->getPost()) && isset($this->_request->id)){

                $id = $this->getRequest()->getParam('id');
                $news = new Model_Static($id);
                $news->fill($form->getValues());
                $news->date  = date('Y-m-d H:i:s');
                $news->save();
                $session->msg = 'Страница изменена';
                $this->_helper->redirector('statics');

            }

        }

        if(isset($this->_request->id)){

            $id = $this->getRequest()->getParam('id');
            $news = new Model_Static($id);

            $form->populate($news->populateForm());

        }

        $this->view->form = $form;

    }


    public function addnewsAction(){


        $this->view->title = "Добавление новости";
        $this->view->headTitle($this->view->title, 'PREPEND');
        $session = new Zend_Session_Namespace('Messages');

        $news = new Model_News();
        $form = new Form_addNews();


        if($this->getRequest()->isPost()){

            if($form->isValid($this->getRequest()->getPost()) &&  !isset($this->_request->id)){
                $data = $form->getValues();
                $news->fill($form->getValues());
                $news->date  = date('Y-m-d H:i:s');

                if($form->picture->getFilename()){

                    $rand_number = rand(0,1000000);

                    preg_match('/.+\/(.+)\.(.+)/',$form->picture->getFilename(),$extention);
                    print_r(realpath(APPLICATION_PATH). '/../public/images/upload/'.$extention[1].'_'.$rand_number.'.'.$extention[2]);

                    rename(realpath(APPLICATION_PATH). '/../public/images/upload/'.$extention[1].'.'.$extention[2],
                        realpath(APPLICATION_PATH). '/../public/images/upload/'.$extention[1].'_'.$rand_number.'.'.$extention[2]);

                    $news->picture = $extention[1].'_'.$rand_number.'.'.$extention[2];

                } else {
                    $news->picture = '';
                }

                $news->save();
                $session->msg = 'Ваши данные изменены';
                $this->_helper->redirector('news');

            } elseif ($form->isValid($this->getRequest()->getPost()) && isset($this->_request->id)){


                $id = $this->getRequest()->getParam('id');
                $news = new Model_News($id);
                $old_picture = $news->picture;
                $news->fill($form->getValues());
                $news->date  = date('Y-m-d H:i:s');
                $news->picture = $old_picture;

                if($form->picture->getFilename()){

                    $rand_number = rand(0,1000000);

                    preg_match('/.+\/(.+)\.(.+)/',$form->picture->getFilename(),$extention);
                    print_r(realpath(APPLICATION_PATH). '/../public/images/upload/'.$extention[1].'_'.$rand_number.'.'.$extention[2]);

                    rename(realpath(APPLICATION_PATH). '/../public/images/upload/'.$extention[1].'.'.$extention[2],
                        realpath(APPLICATION_PATH). '/../public/images/upload/'.$extention[1].'_'.$rand_number.'.'.$extention[2]);

                    $news->picture = $extention[1].'_'.$rand_number.'.'.$extention[2];

                }

                $news->save();
                $session->msg = 'Новость изменена';
                $this->_helper->redirector('news');

            }

        }

        if(isset($this->_request->id)){

            $id = $this->getRequest()->getParam('id');
            $news = new Model_News($id);
            $form->populate($news->populateForm());

        }

        $this->view->form = $form;


    }

    public function deletenewsAction(){
        $session = new Zend_Session_Namespace('Messages');

        if($this->getRequest()->getParam('id')){
            $news = new Model_News($this->getRequest()->getParam('id'));
            if($news->picture){
                $d = unlink(realpath(APPLICATION_PATH). '/../public/images/upload/'.$news->picture);
            }
            $news->delete();
            $session->msg = 'Новость удалена';

            $this->_helper->redirector('news');

        }

    }


    public function addpromoAction(){

        $this->view->title = "Добавление вакансии";
        $this->view->headTitle($this->view->title, 'PREPEND');
        $session = new Zend_Session_Namespace('Messages');

        $news = new Model_Landings();
        $form = new Form_addPromo();


        if($this->getRequest()->isPost()){

            if($form->isValid($this->getRequest()->getPost()) &&  !isset($this->_request->id)){

                $news->fill($form->getValues());

                $news->save();
                $session->msg = 'Данные изменены';
                $this->_helper->redirector('promo');

            } elseif ($form->isValid($this->getRequest()->getPost()) && isset($this->_request->id)){

                $id = $this->getRequest()->getParam('id');
                $news = new Model_Landings($id);

                $news->fill($form->getValues());
                $news->save();
                $session->msg = 'Вакансия изменена';
                $this->_helper->redirector('promo');

            }

        }

        if(isset($this->_request->id)){
            $id = $this->getRequest()->getParam('id');
            $news = new Model_Landings($id);
            $form->populate($news->populateForm());

        }

        $this->view->form = $form;


    }

    public function deletepromoAction(){
        $session = new Zend_Session_Namespace('Messages');

        if($this->getRequest()->getParam('id')){
            $news = new Model_News($this->getRequest()->getParam('id'));
            $news->delete();
            $session->msg = 'Новость удалена';

            $this->_helper->redirector('news');

        }

    }

    public function promoAction(){

        $this->view->title = "Вакансии";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $news = new Model_Landings();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($news->getPromo()));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;

    }

    public function flatsAction(){

        $this->view->title = "Продукты";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $flats = new Model_Flat();

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($flats->getFlats()));
        $paginator->setCurrentPageNumber($this->_getParam('page'));
        $paginator->setItemCountPerPage(50);
        $this->view->paginator = $paginator;


    }

    public function addflatAction()
    {

        $this->view->title = "Добавить новый продукт";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $form = new Form_Flats();
        if($this->getRequest()->isPost()){
            if($form->isValid($this->getRequest()->getPost())){
                $flat = new Model_Flat();
                $frm = $form->getValues();
                $flat->fill($frm);
                $last_id = $flat->save();
                //making images
                if(!empty($frm['images']) && is_array($frm['images'])){
                    foreach($frm['images'] as $im){
                        $rand_number = rand(0,1000000);
                        preg_match('/(.+)\.(.+)/',$im,$extention);
                        rename(realpath(APPLICATION_PATH).'/../public/images/upload/'.$im,
                            realpath(APPLICATION_PATH).'/../public/images/upload/'.$last_id.'_'.$rand_number.'.'.$extention[count($extention)-1]);

                        $photo = new Model_Photo();
                        $photo->flat_id = $last_id;
                        $photo->photo = $last_id.'_'.$rand_number.'.'.$extention[count($extention)-1];
                        $photo->save();
                    }
                }elseif ($frm['images']!=''){
                    $rand_number = rand(0,1000000);
                    preg_match('/(.+)\.(.+)/',$frm['images'],$extention);
                    rename(realpath(APPLICATION_PATH).'/../public/images/upload/'.$frm['images'],
                        realpath(APPLICATION_PATH). '/../public/images/upload/'.$last_id.'_'.$rand_number.'.'.$extention[count($extention)-1]);

                    $photo = new Model_Photo();
                    $photo->flat_id = $last_id;
                    $photo->photo = $last_id.'_'.$rand_number.'.'.$extention[count($extention)-1];
                    $photo->save();

                }
                $this->_helper->redirector('flats');
            }

        }
        $this->view->form = $form;
    }

    public function editflatAction()
    {
        $this->view->title = "Редактировать данные продукта";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $id = $this->getRequest()->getParam('flat_id');
        $flat = new Model_Flat($id);

        if(Zend_Auth::getInstance()->getIdentity()->role != 'admin')
            $this->_helper->redirector('index');

        $last_id = $id;
        $form = new Form_Flats();

        if($this->getRequest()->isPost()){
            if($form->isValid($this->getRequest()->getPost())){
                $frm = $form->getValues();
                $flat->fill($frm);
                $flat->save();
                if(!empty($frm['images']) && is_array($frm['images'])){
                    foreach($frm['images'] as $im){
                        $rand_number = rand(0,1000000);
                        preg_match('/(.+)\.(.+)/',$im,$extention);
                        rename(realpath(APPLICATION_PATH) . '/../public/images/upload/'.$im,
                            realpath(APPLICATION_PATH) . '/../public/images/upload/'.$last_id.'_'.$rand_number.'.'.$extention[count($extention)-1]);

                        $photo = new Model_Photo();
                        $photo->flat_id = $last_id;
                        $photo->photo = $last_id.'_'.$rand_number.'.'.$extention[count($extention)-1];
                        $photo->save();
                    }
                }elseif ($frm['images']!=''){
                    $rand_number = rand(0,1000000);
                    preg_match('/(.+)\.(.+)/',$frm['images'],$extention);
                    rename(realpath(APPLICATION_PATH) . '/../public/images/upload/'.$frm['images'],
                        realpath(APPLICATION_PATH) . '/../public/images/upload/'.$last_id.'_'.$rand_number.'.'.$extention[count($extention)-1]);

                    $photo = new Model_Photo();
                    $photo->flat_id = $last_id;
                    $photo->photo = $last_id.'_'.$rand_number.'.'.$extention[count($extention)-1];
                    $photo->save();

                }
                $this->_helper->redirector('flats');
            }
        } else {
            $form->populate($flat->populateForm());
        }

        $this->view->photos = $flat->getPhotos();
        $this->view->form = $form;
    }

    public function deletephotoAction()
    {
        $id = $this->_getParam('photo_id');
        $photo = new Model_Photo($id);

        unlink(realpath(APPLICATION_PATH . '/../public/images/upload/'.$photo->photo));

        $photo->delete();
        $this->_redirect('user/editflat/flat_id/'.$this->_getParam('flat_id'));

    }

}

