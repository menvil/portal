<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initAutoload() 
	{
		$moduleLoader = new Zend_Application_Module_Autoloader(array( 
			'namespace' => '', 
			'basePath'  => APPLICATION_PATH)); 
		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace(array('Menvil_'));
		return $moduleLoader;
	}


	protected function _initViewHelpers()
	{
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();
		
		$view->headMeta()->appendHttpEquiv('Content-Type', 'text/html;charset=utf-8');
		$view->headTitle('m3Leads');
		$view->headTitle()->setSeparator('  ::  ');

		if(!Zend_Auth::getInstance()->hasIdentity()){
            $view->identity = false;
		}else{
			$view->identity = Zend_Auth::getInstance()->getIdentity();
		}

        $request = new Zend_Controller_Request_Http();

        $registry = Zend_Registry::getInstance();
		$db = $this->bootstrap('db')->getResource('db');
		$db->query('SET NAMES utf8');

       /* */

		Zend_Session::start();


	}
	
	protected function _initPlugins(){
		
		$front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Plugin_Acl());
	}
	
	protected function _initRegistry(){
		$resource = $this->getPluginResource('db');
		Zend_Registry::set('db', $resource->getDbAdapter());

        $registry = Zend_Registry::getInstance();
        $layout = $this->getResource('layout');
        $view = $layout->getView();

        if(!isset($registry->balance) && isset(Zend_Auth::getInstance()->getIdentity()->owner_id)){
            $us = new Model_Owner();

        }

	}
	
}

